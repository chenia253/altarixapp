//
//  ProductTableViewCell.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import UIKit
import Kingfisher

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setProduct(title: String, price: Int, imageURLString: String) {
        titleLabel.text = title
        priceLabel.text = "\(price) ₽"
        guard let imageURL = URL(string: imageURLString) else {return}
        productImageView.kf.setImage(with: imageURL)
    }
}
