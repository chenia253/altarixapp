//
//  AddViewController.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import UIKit
import FirebaseStorage
import SVProgressHUD

class AddViewController: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var imagePicker = UIImagePickerController()
    var pickerData: [String] = []
    var product = Product()
    var productData: ProductData!
    var uniqueId = ""
    var isSelectPhoto = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        setData()
        addPhotoAction()
        checkData()
    }
    
    func setDelegate() {
        pickerView.delegate = self
        pickerView.dataSource = self
        imagePicker.delegate = self
        priceTextField.delegate = self
        titleTextField.delegate = self
    }
    
    func setData() {
        productData = ProductData(id: product.id, title: product.title, price: product.price, productType: product.productType, dealType: product.dealType, imageURL: product.imageURL)
        pickerData = [ProductType.household.rawValue, ProductType.clothes.rawValue, ProductType.appliances.rawValue, ProductType.food.rawValue, ProductType.other.rawValue]
    }
    
    func addPhotoAction() {
        let photoTapped = UITapGestureRecognizer(target: self, action: #selector(selectPhoto))
        productImage.isUserInteractionEnabled = true
        productImage.addGestureRecognizer(photoTapped)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func checkData() {
        if product.id.isEmpty {
            uniqueId = UUID().uuidString
        } else {
            uniqueId = productData.id
        }
        
        if !product.title.isEmpty {
            titleTextField.text = product.title
        }
        if product.price != 0 {
            priceTextField.text = "\(product.price)"
        }
        if let index = pickerData.index(of: product.productType) {
            pickerView.selectRow(index, inComponent: 0, animated: true)
        }
        segmentedControl.selectedSegmentIndex = product.dealType
        if product.productType.isEmpty {
            productData.productType = ProductType.household.rawValue
        }
        guard let imageURL = URL(string: product.imageURL) else {return}
        productImage.kf.setImage(with: imageURL)
    }
    
    @objc func selectPhoto() {
        imagePicker.sourceType = .photoLibrary
        if !isSelectPhoto {
            isSelectPhoto = true
            present(imagePicker, animated: true)
        }
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            productData.dealType = 0
        case 1:
            productData.dealType = 1
        default:
            break
        }
    }
    

    @IBAction func cancelTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func uploadPhoto(image: UIImage) {
        let storageRef = Storage.storage().reference().child(uniqueId)
        
        if let uploadData = image.pngData() {
            let uploadTask = storageRef.putData(uploadData, metadata: nil) { (metaData, error) in
                if error != nil {
                    return
                }
                storageRef.downloadURL { (url, error) in
                    guard let downloadURL = url else { return }
                    self.productData.imageURL = downloadURL.absoluteString
                }
            }
            let observer = uploadTask.observe(.progress) { snapshot in
                DispatchQueue.main.async {
                    if let progress = snapshot.progress?.fractionCompleted {
                        SVProgressHUD.showProgress(Float(progress), status: "Загрузка")
                        if snapshot.status.rawValue == 1 {
                            SVProgressHUD.dismiss()
                            self.productImage.image = image
                            self.isSelectPhoto = false
                            self.saveButton.isEnabled = true
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func saveTapped(_ sender: Any) {
        let newProduct = Product()
        newProduct.id = uniqueId
        if let textPrice = priceTextField.text {
            if let price = Int(textPrice) {
                newProduct.price = price
            }
        }
        if let textTitle = titleTextField.text {
            if textTitle.isEmpty {
                newProduct.title = "Без названия"
            } else {
                newProduct.title = textTitle
            }
        }
        newProduct.productType = productData.productType
        newProduct.dealType = productData.dealType
        newProduct.imageURL = productData.imageURL
        DBManager.shared.persistObject(object: newProduct)
        navigationController?.popViewController(animated: true)
    }
}

extension AddViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image = UIImage()
        saveButton.isEnabled = false
        if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            image = originalImage
        }
        dismiss(animated: true, completion: nil)
        uploadPhoto(image: image)
    }
}

extension AddViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        productData?.productType = pickerData[row]
    }
}

extension AddViewController: UITextFieldDelegate {
    
}
