//
//  ViewController.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var filterButton: UIButton!
    
    var products: [Product] = []
    var filteredProducts: [Product] = []
    var dealType = 0
    var pickerData: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductTableViewCell")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self, action: #selector(addTapped))
        if dealType == 0 {
            filterButton.isHidden = true
        } else {
            filterButton.isHidden = false
        }
        pickerData = [ProductType.household.rawValue, ProductType.clothes.rawValue, ProductType.appliances.rawValue, ProductType.food.rawValue, ProductType.other.rawValue]
        addFewProduct()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        products = DBManager.shared.findObject(type: Product.self, format: "dealType == \(dealType)")
        filteredProducts = products
        tableView.reloadData()
    }
    
    func addFewProduct() {
        for i in 0...7 {
            if i % 2 == 0 {
                createProduct(title: "Стул", imageURL: "https://firebasestorage.googleapis.com/v0/b/altarixapp.appspot.com/o/2185B94C-9768-445A-9835-223011EFC4D5?alt=media&token=3beba776-771b-46f5-a8e0-f55eba6791b7")
            } else {
                createProduct(title: "Бумага", imageURL: "https://firebasestorage.googleapis.com/v0/b/altarixapp.appspot.com/o/060A990D-BD7A-4C85-AD2C-80E831B0DA27?alt=media&token=f35f9ab2-103e-4851-8534-22b9e04820a1")
            }
        }
    }
    
    func createProduct(title: String, imageURL: String) {
        let newProduct = Product()
        newProduct.id = UUID().uuidString
        newProduct.title = title
        newProduct.price = Int.random(in: 200...1000)
        newProduct.productType = pickerData[Int.random(in: 0...4)]
        newProduct.dealType = Int.random(in: 0...1)
        newProduct.imageURL = imageURL
        DBManager.shared.persistObject(object: newProduct)
    }
    
    @objc func addTapped() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "AddViewController") as? AddViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func filterTapped(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Товары для дома", style: .default, handler: { (alertAction) in
            self.products = self.filteredProducts.filter({ (product) -> Bool in
                product.productType == ProductType.household.rawValue
            })
            self.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Одежда", style: .default, handler: { (alertAction) in
            self.products = self.filteredProducts.filter({ (product) -> Bool in
                product.productType == ProductType.clothes.rawValue
            })
            self.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Бытовая техника", style: .default, handler: { (alertAction) in
            self.products = self.filteredProducts.filter({ (product) -> Bool in
                product.productType == ProductType.appliances.rawValue
            })
            self.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Еда", style: .default, handler: { (alertAction) in
            self.products = self.filteredProducts.filter({ (product) -> Bool in
                product.productType == ProductType.food.rawValue
            })
            self.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Другое", style: .default, handler: { (alertAction) in
            self.products = self.filteredProducts.filter({ (product) -> Bool in
                product.productType == ProductType.other.rawValue
            })
            self.tableView.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Отменить фильтр", style: .default, handler: { (alertAction) in
            self.products = self.filteredProducts
            self.tableView.reloadData()
        }))
        self.present(actionSheet, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath) as! ProductTableViewCell
        let product = products[indexPath.row]
        cell.productImageView.image = UIImage(imageLiteralResourceName: "photo")
        cell.setProduct(title: product.title, price: product.price, imageURLString: product.imageURL)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController {
            let product = products[indexPath.row]
            vc.product = product
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            dealType = 0
            filterButton.isHidden = true
            products = DBManager.shared.findObject(type: Product.self, format: "dealType == 0")
            filteredProducts = products
            tableView.reloadData()
        case 1:
            dealType = 1
            filterButton.isHidden = false
            products = DBManager.shared.findObject(type: Product.self, format: "dealType == 1")
            filteredProducts = products
            tableView.reloadData()
        default:
            break
        }
    }
}

