//
//  DetailViewController.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productTypeLabel: UILabel!
    
    var product = Product()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Изменить", style: UIBarButtonItem.Style.plain, target: self, action: #selector(editTapped))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let id = product.id
        if let newProduct = DBManager.shared.findObject(type: Product.self, format: "id == '\(id)'").first {
            setProduct(product: newProduct)
        }
        
    }
    
    @objc func editTapped() {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "AddViewController") as? AddViewController {
            vc.product = product
            vc.title = "Изменение товара"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setProduct(product: Product) {
        productTitleLabel.text = product.title
        productPriceLabel.text = "\(product.price) ₽"
        productTypeLabel.text = product.productType
        guard let imageURL = URL(string: product.imageURL) else {return}
        productImage.kf.setImage(with: imageURL)
    }
    
}
