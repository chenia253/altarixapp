//
//  DBManager.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    public static let shared = DBManager()
    
    var realm: Realm!
    
    private init() {
        let config = Realm.Configuration(deleteRealmIfMigrationNeeded: true)
        Realm.Configuration.defaultConfiguration = config
        do {
            realm = try Realm()
        } catch {
            ErrorHandler.shared.handle(error: error.localizedDescription)
        }
    }
    
//    func getFilms() -> [Films] {
//        return realm.objects(Films.self).shuffled()
//    }
    
    func persistObject(object: Object) {
        do {
            try realm.write {
                if type(of: object).primaryKey() != nil {
                    realm.add(object, update: Realm.UpdatePolicy.modified)
                } else {
                    realm.add(object)
                }
            }
        } catch {
            ErrorHandler.shared.handle(error: error.localizedDescription)
        }
    }
    
    func persistObjects(objects: [Object]) {
        do {
            try realm.write {
                for object in objects {
                    if type(of: object).primaryKey() != nil {
                        realm.add(object, update: Realm.UpdatePolicy.modified)
                    } else {
                        realm.add(object)
                    }
                }
            }
        } catch {
            ErrorHandler.shared.handle(error: error as! String)
        }
    }
    
    func findObject<T>(type: T.Type, format: String) -> [T] {
        return Array(realm.objects(type as! Object.Type).filter(NSPredicate(format: format))) as! [T]
    }
    
    func allObjects<T>(type: T.Type) -> [T] {
        return Array(realm.objects(type as! Object.Type)) as! [T]
    }
    
    func deleteObjects(objects: [Object]) {
        do {
            try realm.write {
                for object in objects {
                    realm.delete(object)
                }
            }
        } catch {
            ErrorHandler.shared.handle(error: error.localizedDescription)
        }
    }
}
