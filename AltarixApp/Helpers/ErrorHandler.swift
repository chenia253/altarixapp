//
//  ErrorHandler.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import Foundation
class ErrorHandler {
    
    static let shared = ErrorHandler()
    
    private init() {}
    
    func handle(error: String) {
        print("error handler called")
        print(error)
    }
    
    func handler(error: Error) {
        print("error handler called")
        print(error)
    }
}
