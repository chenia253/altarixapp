//
//  Product.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import Foundation
import RealmSwift

class Product: Object {
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var price = 0
    @objc dynamic var productType = ""
    @objc dynamic var dealType = 0 // 0 - buy, 1 - sell
    @objc dynamic var imageURL = ""
    
    override open class func primaryKey() -> String? {
        return "id"
    }
}
