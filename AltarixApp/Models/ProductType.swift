//
//  ProductType.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 07/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import Foundation

enum ProductType: String {
    case household = "Товары для дома"
    case appliances = "Бытовая техника"
    case food = "Еда"
    case clothes = "Одежда"
    case other = "Другое"
}
