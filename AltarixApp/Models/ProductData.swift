//
//  Product.swift
//  AltarixApp
//
//  Created by Денис Лапшин on 09/08/2019.
//  Copyright © 2019 Денис Лапшин. All rights reserved.
//

import Foundation

class ProductData {
    var id = ""
    var title = ""
    var price = 0
    var productType = ""
    var dealType = 0 // 0 - buy, 1 - sell
    var imageURL = ""
    
    init(id: String, title: String, price: Int, productType: String, dealType: Int, imageURL: String) {
        self.id = id
        self.title = title
        self.price = price
        self.productType = productType
        self.dealType = dealType
        self.imageURL = imageURL
    }
}
